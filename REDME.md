# 2D DMP
## Random broken line path
<figure>
<img src="pictures/2D_random1.png" alt="2D_random1.png" width="33%">
<img src="pictures/2D_random2.png" alt="2D_random2.png" width="33%">
<img src="pictures/2D_random3.png" alt="2D_random3png" width="33%">
</figure>

## Hand drawing path
<figure>
<img src="s.png" alt="s.png" width="33%">
<img src="3.png" alt="3.png" width="33%">
<img src="broken_line.png" alt="broken_line.png" width="33%">
</figure>

<figure>
<img src="pictures/handwriting2.png" alt="handwriting2.png" width="33%">
<img src="pictures/handwriting1.png" alt="handwriting1.png" width="33%">
<img src="pictures/handdrawing.png" alt="handdrawing.png" width="33%">
</figure>

## Goal change
![image info](pictures/2.png)

"When switching the goal $g$ to a new value, there exists a discontinuous jump in the acceleration $\ddot{y}$. This can be avoided by filtering the goal change with a simple first-order differential equation: $\tau \dot{g}=\alpha_g(g_0-g)$ 
In this formulation, $g_0$ is the discontinuous goal change, while $g$ is now a continuous variable. This modification does not affect the scaling properties and stability properties of the system and is easily incorporated in the learning algorithm with LWR."
(Ijspeert et al., 2013, p. 346)

### goal change to :(1.5,-1), (2,-3),(0.3,-0.7), with $\alpha_g=1,\tau=1$
<figure>
<img src="pictures/goal:1.5,-1.png" alt="goal:1.5,-1.png" width="33%">
<img src="pictures/goal:2,-3.png" alt="goal:2,-3.png" width="33%">
<img src="pictures/goal:0.3,-0.7.png" alt="goal:0.3,-0.7.png" width="33%">
<img src="pictures/arriving1.png" alt="arriving1.png" width="50%">
</figure>

### goal change to :(1.5,-1), (2,-3),(0.3,-0.7), with $\alpha_g=5,\tau=1$
<figure>
<img src="pictures/goal:1.5,-1,ag=5,tau=1.png" alt="goal:1.5,-1,ag=5,tau=1.png" width="33%">
<img src="pictures/goal:2,-3,ag=5,tau=1.png" alt="goal:2,-3,ag=5,tau=1.png" width="33%">
<img src="pictures/goal:0.3,-0.7,ag=5,tau=1.png" alt="goal:0.3,-0.7,ag=5,tau=1.png" width="33%">
<img src="pictures/arriving2.png" alt="arriving2.png" width="50%">
</figure>



# 3D DMP
![image info](pictures/3D_criticallydamped.png)
* goal change to :(1.5,-1,1), (-9,-11,3)
<figure>
<img src="pictures/3D_ag=5,goal=1.5,-1,1.png" alt="3D_ag=5,goal=1.5,-1,1.png" width="100%">
<img src="pictures/3Darriving1.png" alt="3Darriving1.png" width="50%">
<img src="pictures/3D_ag=5,goal=-9,-11,3.png" alt="3D_ag=5,goal=-9,-11,3.png" width="100%">
<img src="pictures/3Darriving2.png" alt="3Darriving2.png" width="50%">
</figure>
