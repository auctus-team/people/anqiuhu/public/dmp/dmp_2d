import matplotlib.pyplot as plt
import numpy as np
import DMP_discrete



t = np.linspace(0, 10, 1000)
# Adjusting the formula to ensure the start point is at (0, 0, 0)
x1 = t * np.sin(t)
y1 = t * np.cos(t)
z1 = np.log(t + 1)  # Use log to ensure z starts from 0 and changes distinctly
y_des1 = (np.stack((x1,y1,z1),axis=1)).T


theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
z2 = np.linspace(-2, 2, 100)
r = z2**2 + 1
x2 = r * np.sin(theta)
y2 = r * np.cos(theta)
y_des2 = (np.stack((x2,y2,z2),axis=1)).T  

dmp = DMP_discrete.DMP_discrete(n_dmps=3, n_bfs=1000, ay=np.ones(3) * 24.0)
dmp.imitate_path(y_des=y_des1)
y_track, dy_track, ddy_track = dmp.system(tau=1)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Plotting both spirals
ax.plot(y_des1[0,:], y_des1[1,:],y_des1[2,:], label='desired path')
ax.plot(y_track[:, 0], y_track[:, 1], y_track[:, 2], label='imitate path', color='r')  # Second spiral in red for contrast


y_track = []
dmp.reset_state()
new_goal=[-9,-11,3]
tau=1
for t in range(dmp.timesteps):
    y, _, _ = dmp.step()
    y_track.append(np.copy(y))
    # move the target slightly every time step
    #dmp.goal += np.array([1e-2, 1e-2])
    dmp.goal += (dmp.ag * (new_goal-dmp.goal) / tau )* dmp.dt
y_track = np.array(y_track)
ax.plot(y_track[:, 0], y_track[:, 1],y_track[:, 2], "y", label="moving target")
print("trajectory arrive at:",y_track[-1,:])

plt.title("DMP imitate path")
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
ax.legend()

plt.show()


