import numpy as np
import matplotlib.pyplot as plt
import DMP_discrete
import cv2

#desired path from image
image = cv2.imread('/home/anqiu/dmp_2d/3.png', cv2.IMREAD_GRAYSCALE)
edges = cv2.Canny(image, threshold1=200, threshold2=200)
y_des1 = np.argwhere(edges == 255)
y_des1 = (y_des1-y_des1[0]).T #shift start point at (0,0)

#desired path from python function
x1 = np.linspace(0.0, 5.0, 100)
y1 = np.cos(2 * np.pi * x1) * np.exp(-x1)

x2 = np.random.randint(6, size=6)
y2 = np.random.randint(6, size=6)

y_des2 = np.stack((x2,y2),axis=1)
y_des2 = (y_des2-y_des2[0]).T


#desired path from database
y_des3 = np.load("2.npz")["arr_0"].T
y_des3 -= y_des3[:, 0][:, None]




dmp = DMP_discrete.DMP_discrete(n_dmps=2, n_bfs=1000, ay=np.ones(2) * 24)


dmp.imitate_path(y_des=y_des3)
y_track, dy_track, ddy_track = dmp.system(tau=1)

plt.figure()
plt.plot(y_des3[0,:], y_des3[1,:],"r", label='desired path')
plt.plot(y_track[:, 0], y_track[:, 1], "b", label='imitate path')



y_track = []
dmp.reset_state()
new_goal=[0.3,-0.7]
tau=1
for t in range(dmp.timesteps):
    y, _, _ = dmp.step()
    y_track.append(np.copy(y))
    # move the target slightly every time step
    #dmp.goal += np.array([1e-2, 1e-2])
    dmp.goal += (dmp.ag * (new_goal-dmp.goal) / tau )* dmp.dt
y_track = np.array(y_track)
plt.plot(y_track[:, 0], y_track[:, 1], "y", label="moving target")
print("trajectory arrive at:",y_track[-1,:])


plt.xlabel("X axis")
plt.ylabel("Y axis")
plt.title("DMP imitate path")
plt.legend()
plt.show()
